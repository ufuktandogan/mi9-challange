var request = require('supertest')
  , async = require('async')
  , should = require('should')
  , app = require('../index.js').app
  , validPayload = require('./helpers/validPayload.json')
  , invalidPayload = require('./helpers/invalidPayload.json');

  describe('POST /', function() {
    it('should respond success', function (next) {
    request(app)
      .post('/')
      .send(validPayload)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, function (err, res) {
        if (err) return next(err);
        return next()
      });
    });

    it('should print down invalid attributes', function (next) {
    request(app)
      .post('/')
      .send(invalidPayload)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200, function (err, res) {
        if (err) return next(err);
        return next()
      });
    });

    it('should respond with error', function (next) {
    request(app)
      .post('/')
      .send('{"invalid"}')
      .type('json')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400, function (err, res) {
        if (err) return next(err);
        return next()
      });
    });
  });