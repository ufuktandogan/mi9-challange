var express = require('express')
, app = express()
, async = require('async')
, bodyParser = require('body-parser')
, validator = require('jsonschema').Validator
, methodOverride = require('method-override')
, schema = require('./paramSchema.json')
, v = new validator();
//loading dependencies including JSON validator

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(errorHandler);

app.set('port', (process.env.PORT || 3000));

//handles all kind of errors and informs with its explanation
function errorHandler(err, req, res, next) {
  console.log('something bad happened', err);
  if (err instanceof SyntaxError) {
    return res.status(400).json({
      'error': 'Could not decode request: JSON parsing failed'
    })
  }
  return res.status(500).json({
    'error': err
  });
}

app.post('/', function(req, res, next) {
  var params = req.body
   ,  response = [];

  if (!('payload' in params) || params['payload'].constructor != Array) {
    return res.status(400).json({
      'error': 'You should put your data in payload object'
    })
  }

  async.eachSeries(params['payload'], function iteratee(item, next) {
    //validates given JSON object with JSON schema.
    var validation = v.validate(item, schema);
    if (!validation.valid) console.log(validation.errors);

    if (validation.valid && item['drm'] === true) {
      newResponseObject = {
        image: item['image']['showImage'],
        slug: item['slug'],
        title: item['title']
      };
      response.push(newResponseObject);
    }
    return next();
  }, function done() {
    res.json({response:response});
  });
});

app.listen(app.get('port'), function () {
  console.log('Challenge app listening on port 3000!');
});

module.exports.app = app;

