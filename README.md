9Digital Coding Challenge

Installation
------------
cd mi9-coding-challenge
npm install

Run
----
npm start

Test
----
npm install mocha -g
npm test

After npm start, API server is ready to response requests.

Sending request to API on local
-------------------------------
-testing endpoint without json
curl -X POST -d 'invalid' -H "Content-Type: application/json" localhost:3000

-testing endpoint with invalid payload
curl -X POST -H 'Content-Type:application/json' -H 'Accept: application/json' --data-binary @test/helpers/invalidPayload.json http://localhost:3000 

-testing endpoint with valid payload
curl -X POST -H 'Content-Type:application/json' -H 'Accept: application/json' --data-binary @test/helpers/validPayload.json http://localhost:3000